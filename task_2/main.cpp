#include <iostream>
using namespace std;
// #include <stdio.h>

template <class T>
class BigInteger {
    T a, b;
  public:
    BigInteger (T const first, T const second)
      {a=first; b=second;}
    T add ();
    T subtract ();
    T multiply ();
};

template <class T>
T BigInteger<T>::add ()
{
    T retval;
    retval = a + b;
    return retval;
}

template <class T>
T BigInteger<T>::subtract ()
{
    T retval;
    retval = a - b;
    return retval;
}

template <class T>
T BigInteger<T>::multiply ()
{
    T retval;
    retval = a * b;
    return retval;
}

int main()
{
    double x=5, y=6; 
    BigInteger <int> a (x, y);
    BigInteger <int> s (x, y);
    BigInteger <int> m (x, y);


    cout << a.add() << endl;
    cout << s.subtract() << endl;
    cout << m.multiply() << endl;
    
    return 0;
}
