import random
import asyncio


class Sender(asyncio.DatagramProtocol):
    def connection_made(self, transport) -> None:
        randint = random.randint(0, 10000)
        transport.sendto(str(randint).encode())


class Listener(asyncio.DatagramProtocol):
    def datagram_received(self, data: bytes, addr: str) -> None:
        print(data)


async def send(loop, addr, sender_port):
    while True:
        loop.create_datagram_endpoint(Sender, remote_addr=(addr, sender_port))
        await asyncio.sleep(1)


def run(addr='127.0.0.1', sender_port: int=11234, listener_port: int=11235) -> None:
    loop = asyncio.get_event_loop()
    listener = loop.create_datagram_endpoint(Listener, local_addr=(addr, listener_port))
    asyncio.gather(*[send(loop, addr, sender_port), listener], loop=loop)
    loop.run_forever()


if __name__ == '__main__':
    try:
        run()
    except KeyboardInterrupt:
        pass
