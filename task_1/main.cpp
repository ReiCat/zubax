#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

unsigned powmod(unsigned b, unsigned p, unsigned m)
{
    unsigned res = 1;    
    while (p != 0) 
    {
        if ((p & 1) != 0)
        {
            res = (1ll * res * b) % m;
        }        
        b = (1ll * b * b) % m;
        p >>= 1;
    }    
    return res;
}

TEST_CASE( "Modular exponentiation computation" ) {
    REQUIRE( powmod(131999920, 234979, 991540365) == 682771105 );
    REQUIRE( powmod(1, 4, 497) == 1 );
    REQUIRE( powmod(63437, 0, 20628) == 1 );
}