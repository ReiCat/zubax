## task_1

Build instructions:
```
g++ task_1/main.cpp -Werror -Wall -Wextra -pedantic -std=c++17
```

## task_2 (not finished)

Build instructions:
```
g++ task_2/main.cpp -Werror -Wall -Wextra -pedantic -std=c++17
```

## task_3

Run instructions:
```
python task_3/main.py
```
